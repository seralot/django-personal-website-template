import personal_website.webapp.views


class TestHomeView:
    def test_render(self, rf):
        # Arrange
        request = rf.get("/")

        # Act
        response = personal_website.webapp.views.home(request)

        # Assert
        assert response.status_code == 200
