# Django personal website template

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

---

## Descripción

Plantilla de Django diseñada para crear fácilmente una web personal.

## Licencia

Este proyecto está licenciado bajo la GNU General Public License 3.0. Consulta el archivo [LICENSE](https://gitlab.com/seralot/django-personal-website-template/-/blob/main/LICENSE) para obtener más detalles
