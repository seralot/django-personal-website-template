from django.http import HttpRequest
from django.shortcuts import HttpResponse


def home(request: HttpRequest) -> HttpResponse:
    return HttpResponse("<h1>Home</h1>")
