from django.urls import path

import personal_website.webapp.views

urlpatterns = [
    path("", personal_website.webapp.views.home, name="home"),
]
