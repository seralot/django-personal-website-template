from .base import *  # noqa

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "django-insecure-q(p7(ys3-v%pa9u#tkld%sar9)u8ptn09o3gvrdk4lw0@a)9uc"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []
